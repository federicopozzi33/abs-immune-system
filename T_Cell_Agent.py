from mesa import Agent
import random
from random import choice


class T_Cell_Agent(Agent):
    """
        T Cell Agent

        Parameters
        ----------
        unique_id: int
        model: Model
        bit_string: string

        Other Parameters
        ----------------
        can_bind: boolean (default True)
        is_binded_with: reference to B Cell Agent obj (default None)
        divide_T_max: int (default 3)
        divide_T_counter: int (default 0)
        """
    def __init__(self, unique_id, model, bit_string):
        super().__init__(unique_id, model)
        self.unique_id = unique_id
        self.bit_string = bit_string
        self.can_bind = True
        self.is_binded_with = None
        # Maximum number of steps the cell can divide
        self.divide_T_max = 3
        # number of cell to create at each step during the reproduction phase
        self.cells_to_create = '124'
        # Parameter to keep track of how many times the cell has divided
        self.divide_T_counter = 0
        self.state = 'naive'

    def get_class_name(self):
        return 'T_Cell_Agent' if self.state == 'naive' else 'MT_Cell_Agent'

    def move(self):
        if self.can_bind:
            possible_steps = self.model.grid.get_neighborhood(self.pos, moore=True, include_center=False)
            new_position = random.choice(possible_steps)
            self.model.grid.move_agent(self, new_position)
