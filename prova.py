from scipy import special
import numpy as np
from scipy.stats import norm
from pickle_functions import *
import matplotlib as plt

# n_bits = 8
# prob = []
# aff_enhance = 2.5
# aff_level = 0.05
# minMatch = 6
# for i in range(0, 9):
#      # print(i)
#     if i < minMatch:
#         prob.append(0)
#     elif i == minMatch:
#         prob.append(aff_level)
#     else:
#         temp = aff_enhance * (special.binom(n_bits, i - 1) / special.binom(n_bits, i)) * prob[i - 1]
#         prob.append(temp)
# # print(prob)
# # print(prob / prob[8])
# print(np.exp(np.log(2) * (-1) / 50))

# for i in range(0,10):
#     prova = [0,0]
#     if i > 15:
#         print('ciao')
# else:
#     print('ecco')


# def calculate_moving_prob(n_agents, max_capacity):
#     res = list(map(lambda i: norm.pdf(i, 0, max_capacity/3), n_agents))
#     for i, j in enumerate(n_agents):
#         if j >= max_capacity:
#             res[i] = 0
#     return np.array(res) / sum(res)
#
#
# n_agents = np.array([3, 42, 15, 12, 4, 33, 50, 9, 21])
# res = calculate_moving_prob(n_agents, 50)
# print(res)
# variance = sum(list(map(lambda i : (i - min(n_agents)**2) / len(n_agents), n_agents)))

# variance = list(map())
# Media: valore minimo dell'array (così la casella con probabilità massima è quella con capacità minore)



# res = list(map(lambda i: norm.pdf(i, 0, 1), probs))
#
# print(res)
#

data = load_data_with_pickle("simulation_data.pickle")
print(data[-2])

# [n_step, [
#   [n_agents]
#   [antibody],
#   [antigen],
#   [t_cell, t_cell_binded_with_b, t_cell_binded_with_apc, t_memory, t_naive, t_cell_compatible, t_cell_dividing],
#   [apc, apc_binded],
#   [b_cell, b_cell_compatible, b_cell_plasma, b_cell_memory, b_cell_naive, b_cell_1_2, b_cell_2_2, b_cell_dividing]]
#   [dead_agents_during_step],
#   [new_agents_from_bone_marrow],
#   [n_antigens_killed_by_antibody, n_antigens_killed_by_antibody_in_this_step],
#   [n_antigens_killed_by_B, n_antigens_killed_by_B_in_this_step],
#   [n_antigens_killed_by_apc, n_antigens_killed_by_apc_in_this_step]
# ]

t_cell = [x[1][3][0] for x in data]
t_cell_binded_with_b = [x[1][3][1] for x in data]
t_cell_binded_with_apc = [x[1][3][2] for x in data]
t_memory = [x[1][3][3] for x in data]
t_naive = [x[1][3][4] for x in data]
t_cell_compatible = [x[1][3][5] for x in data]
t_cell_dividing = [x[1][3][6] for x in data]

apc = [x[1][4][0] for x in data]
apc_binded = [x[1][4][1] for x in data]
step = [x[0] for x in data]

antibody = [x[1][1] for x in data]
antigen = [x[1][2] for x in data]

b_cell = [x[1][5][0] for x in data]
b_cell_compatible = [x[1][5][1] for x in data]
b_cell_plasma = [x[1][5][2] for x in data]
b_cell_memory = [x[1][5][3] for x in data]
b_cell_naive = [x[1][5][4] for x in data]
b_cell_1_2 = [x[1][5][5] for x in data]
b_cell_2_2 = [x[1][5][6] for x in data]
b_cell_dividing = [x[1][5][7] for x in data]

n_antigens_killed_by_antibody = [x[4][0] for x in data[:-1]]
n_antigens_killed_by_antibody_in_this_step = [x[4][1] for x in data[:-1]]
n_antigens_killed_by_antibody.append(n_antigens_killed_by_antibody[-1])
n_antigens_killed_by_antibody_in_this_step.append(n_antigens_killed_by_antibody_in_this_step[-1])

n_antigens_killed_by_B = [x[5][0] for x in data[:-1]]
n_antigens_killed_by_B_in_this_step = [x[5][1] for x in data[:-1]]
n_antigens_killed_by_B.append(n_antigens_killed_by_B[-1])
n_antigens_killed_by_B_in_this_step.append(n_antigens_killed_by_B_in_this_step[-1])

n_antigens_killed_by_apc = [x[6][0] for x in data[:-1]]
n_antigens_killed_by_apc_in_this_step = [x[6][1] for x in data[:-1]]
n_antigens_killed_by_apc.append(n_antigens_killed_by_apc[-1])
n_antigens_killed_by_apc_in_this_step.append(n_antigens_killed_by_apc_in_this_step[-1])



