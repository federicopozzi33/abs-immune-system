from mesa import Agent
import random
from random import choice


class Dying_Agent(Agent):
    """
        Dying Agent (represents dead Antigens)

        Parameters
        ----------
        unique_id: int
        model: Model

        Other Parameters
        ----------------
        count: int (default 1)
        Counter variable to count after how many steps the Dying Agent will be remove definitively from the simulation
        """
    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.unique_id = unique_id
        self.count = 1

    def move(self):
        possible_steps = self.model.grid.get_neighborhood(self.pos, moore=True, include_center=False)
        new_position = random.choice(possible_steps)
        self.model.grid.move_agent(self, new_position)

    def get_class_name(self):
        return 'Dying_Agent'
