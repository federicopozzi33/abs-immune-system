from pickle_functions import *
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

data = load_data_with_pickle("simulation_data.pickle")

t_cell = [x[1][3][0] for x in data]
t_cell_binded_with_b = [x[1][3][1] for x in data]
t_cell_binded_with_apc = [x[1][3][2] for x in data]
t_memory = [x[1][3][3] for x in data]
t_naive = [x[1][3][4] for x in data]
t_cell_compatible = [x[1][3][5] for x in data]
t_cell_dividing = [x[1][3][6] for x in data]

apc = [x[1][4][0] for x in data]
apc_binded = [x[1][4][1] for x in data]
step = [x[0] for x in data]

antibody = [x[1][1][0] for x in data]
antigen = [x[1][2][0] for x in data]

b_cell = [x[1][5][0] for x in data]
b_cell_compatible = [x[1][5][1] for x in data]
b_cell_plasma = [x[1][5][2] for x in data]
b_cell_memory = [x[1][5][3] for x in data]
b_cell_naive = [x[1][5][4] for x in data]
b_cell_1_2 = [x[1][5][5] for x in data]
b_cell_2_2 = [x[1][5][6] for x in data]
b_cell_dividing = [x[1][5][7] for x in data]

n_antigens_killed_by_antibody = [x[4][0] for x in data[:-1]]
n_antigens_killed_by_antibody_in_this_step = [x[4][1] for x in data[:-1]]
n_antigens_killed_by_antibody.append(n_antigens_killed_by_antibody[-1])
n_antigens_killed_by_antibody_in_this_step.append(n_antigens_killed_by_antibody_in_this_step[-1])

n_antigens_killed_by_B = [x[5][0] for x in data[:-1]]
n_antigens_killed_by_B_in_this_step = [x[5][1] for x in data[:-1]]
n_antigens_killed_by_B.append(n_antigens_killed_by_B[-1])
n_antigens_killed_by_B_in_this_step.append(n_antigens_killed_by_B_in_this_step[-1])

n_antigens_killed_by_apc = [x[6][0] for x in data[:-1]]
n_antigens_killed_by_apc_in_this_step = [x[6][1] for x in data[:-1]]
n_antigens_killed_by_apc.append(n_antigens_killed_by_apc[-1])
n_antigens_killed_by_apc_in_this_step.append(n_antigens_killed_by_apc_in_this_step[-1])

sns.set_style("whitegrid")

fig = plt.figure()
#ax = fig.add_subplot(111)

approach = step
# recall = np.array([32.8, 43.8, 72.9])
# error_rate_before = np.array([37.4, 37.4, 37.4])
# error_rate_after = np.array([26.3, 22.2, 11.5])

antibody_color = '#990099'
antigen_color = '#000000'
apc_color = '#3366ff'
apc_binded_color = ' #3333ff'
b_cell_color = '#cc0000'
b_cell_compatible_color = '#ff6633'
b_memory_color = '#990000'
b_plasma_color = '#ff3300'
b_naive_color = '#ff9933'

t_cell_color = '#009900'
t_cell_compatible_color = '#00cc00'
t_naive_color = '#00cc33'
t_memory_color = '#006600'
t_binded_apc_color = '#00cc66'
t_binded_b_color = '#ff9966'

killed_by_b_color = '#330000'
killed_by_apc_color = '#000033'
killed_by_ab_color = '#330033'

# plt.plot(approach, b_cell, label='# b-cell', linewidth=2.5,
#          color=b_cell_color)
# plt.plot(approach, b_cell_compatible, label='# b-cell compatible', linewidth=2.5, color=b_cell_compatible_color)
# plt.plot(approach, t_cell, label='# t-cell', linewidth=2.5,  color=t_cell_color)
# plt.plot(approach, t_cell_compatible, label='# t-cell compatible', linewidth=2.5, color=t_cell_compatible_color)
#
# y_lim = max(max(b_cell), max(b_cell_compatible), max(t_cell_compatible), max(t_cell) + 10000)
# plt.axis([0, len(approach), 0, 10000])
#
# plt.xlabel('Steps')
# plt.ylabel('# of agents')
#
#
# ax2 = plt.axes()
# ax2.xaxis.grid(False)
# plt.legend()
# # plt.title('500k words, ', y=1.05)
# fig.set_size_inches(10, 6)
# plt.savefig('plots/sim1_g2', dpi=800)


def custom_plot(step, list_agents, list_colors, list_labels, max, filename):
    fig = plt.figure()
    # ax = fig.add_subplot(111)

    for a, c, l in zip(list_agents, list_colors, list_labels):
        plt.plot(step, a, label=l, linewidth=2.5, color=c)

    plt.axis([26, 25 + len(step), 0, 100])
    plt.xlabel('Steps')
    plt.ylabel('# of agents')
    # plt.ylabel('Error - Logarithmic Scale')
    # plt.annotate(str('- 103%'), xy=(2.02, error_rate_after[2]+6), ha='center', va='center', size=10, horizontalalignment='right')
    # plt.annotate(str('+ 130%'), xy=(2.02, recall[2]+6), ha='center', va='center', size=10, horizontalalignment='right')

    # for i, j in zip(recall, error_rate_after):
    #         plt.annotate(str(j), xy=(, 1), ha='center', va='center', size=10, horizontalalignment='right')
    ax2 = plt.axes()
    ax2.xaxis.grid(False)
    plt.legend(frameon=True)
    # plt.title('500k words, ', y=1.05)
    fig.set_size_inches(10, 6)
    # plt.show()
    #plt.show()
    plt.savefig('plots/'+ filename + '.png', dpi=800)


# SIM_1_G1
# custom_plot(step, list_agents, list_colors, list_labels, max, 'sim1_g2')
# SIM 1 G9
list_agents = [t_cell_binded_with_apc[25:70], t_cell_binded_with_b[25:70], t_cell_compatible[25:70],
               b_cell_compatible[25:70], t_cell[25:70], b_cell[25:70], apc[25:70]]

max = max(list(map(max, list_agents)))

list_colors = [t_binded_apc_color, t_binded_b_color, t_cell_compatible_color, b_cell_compatible_color, t_cell_color,
               b_cell_color, apc_color]
list_labels = ['# t-cell binded with APC', '# t-cell binded with B', '# t-cell compatible', '# b-cell compatible',
               '# t-cell', '# b-cell']
custom_plot(step[25:70], list_agents, list_colors, list_labels, max, 'sim_0_0_g9_first_peak')


# SIM 1 G8
# list_agents = [b_cell_2_2[25:70], b_cell_plasma[25:70], antibody[25:70]]
#
# max = max(list(map(max, list_agents)))
#
# list_colors = [b_cell_color, b_plasma_color, antibody_color]
# list_labels = ['# b-cell interacted with B and T', '# b-cell plasma', '# antibody']
# custom_plot(step[25:70], list_agents, list_colors, list_labels, max, 'sim_0_0_g8_first peak')

# # SIM 1 G7
# list_agents = [b_cell, b_cell_dividing, t_cell, t_cell_dividing]
#
# max = max(list(map(max, list_agents)))
#
# list_colors = [b_cell_color, b_naive_color,
#                t_cell_color, t_naive_color]
# list_labels = ['# b-cell', '# b-cell dividing',
#                '# t-cell', '# t-cell dividing'
#                ]
# custom_plot(step, list_agents, list_colors, list_labels, max, 'sim1_g7')

# # SIM 1 G6
# list_agents = [b_cell_1_2, n_antigens_killed_by_B_in_this_step]
#
# max = max(list(map(max, list_agents)))
#
# list_colors = [b_naive_color, killed_by_b_color]
# list_labels = ['# b-cell interacted with antigen', '# antigen killed by b'
#                ]
# custom_plot(step, list_agents, list_colors, list_labels, max, 'sim1_g6')

# SIM 2 G1
# list_agents = [antigen, antibody, t_cell, b_cell, apc]

# max = max(list(map(max, list_agents)))
#
# list_colors = [antigen_color, antibody_color, t_cell_color, b_cell_color, apc_color]
# list_labels = ['# antigen', '# antibody', '# t-cell', '# b-cell',
#                '# apc']
# custom_plot(step, list_agents, list_colors, list_labels, max, 'sim_0_300_g1')