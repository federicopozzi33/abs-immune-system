from mesa import Agent
import random
from random import choice

class Antigen_Agent(Agent):
    """
        Antigen Agent

        Parameters
        ----------
        unique_id: int
        model: Model
        bit_string: string
        alive: boolean (default True)

        Other Parameters
        ----------------
        has_interacted: boolean (default False)
        antigen_bit_string: string (default '')
        is_binded_with: reference to Antigen obj (default None)
        """
    def __init__(self, unique_id, model, bit_string, peptide):
        super().__init__(unique_id, model)
        self.unique_id = unique_id
        # Antigen epitope
        self.bit_string = bit_string
        # Peptide bit string
        self.peptide = peptide
        self.alive = True


    def move(self):
        possible_steps = self.model.grid.get_neighborhood(self.pos, moore=True, include_center=False)
        new_position = random.choice(possible_steps)
        self.model.grid.move_agent(self, new_position)

    def get_class_name(self):
        return 'Antigen_Agent'
