from mesa import Agent
import random
from random import choice


class AP_Cell_Agent(Agent):
    """
        AP Cell Agent

        Parameters
        ----------
        unique_id: int
        model: Model

        Other Parameters
        ----------------
        has_interacted: boolean (default False)
        antigen_bit_string: string (default '')
        is_binded_with: reference to Antigen obj (default None)
        """
    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.unique_id = unique_id
        # Flag that holds the interaction with an antigen
        self.has_interacted = False
        self.mpc = ''
        self.is_binded_with = None

    def get_class_name(self):
        return 'AP_Cell_Agent'

    def move(self):
        possible_steps = self.model.grid.get_neighborhood(self.pos, moore=True, include_center=False)
        new_position = random.choice(possible_steps)
        self.model.grid.move_agent(self, new_position)
        if self.is_binded_with is not None:
            self.model.grid.move_agent(self.is_binded_with, new_position)
