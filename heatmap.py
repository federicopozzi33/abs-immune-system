import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from pickle_functions import *
from collections import Counter
sns.set()

# B Cells: 1000 | A Cells: 247 | AB Cells: 716 | T Cells: 1000 | AP Cells: 942

#pos = load_data_with_pickle('b_cell_pos_step1.pickle')
#print(pos[:30])

def plot_heatmap(path, freq):
    pos = load_data_with_pickle(path)
    print(pos[:20])
    pos = [b for b in pos if b]
    #print(pos)
    empty_matrix = np.zeros((50, 50))

    for p in pos:
        empty_matrix[p[0][0], p[0][1]] += len(pos)
    print('MAX VALUE: {}'.format(np.max(empty_matrix)))
    #empty_matrix = empty_matrix/freq
    print('MAX VALUE: {}'.format(np.max(empty_matrix)))
    ax = sns.heatmap(empty_matrix)
    plt.show()

def plot_heatmap_step1(path, freq):
    pos = load_data_with_pickle(path)
    pos = [b for b in pos if b]
    print(pos)
    empty_matrix = np.zeros((50, 50))
    for p in pos:
        empty_matrix[p[0], p[1]] += len(pos)
    print('MAX VALUE: {}'.format(np.max(empty_matrix)))
    #empty_matrix = empty_matrix/freq
    print('MAX VALUE: {}'.format(np.max(empty_matrix)))
    ax = sns.heatmap(empty_matrix)
    plt.show()

plot_heatmap_step1('t_cell_pos_step1.pickle', 1)
plot_heatmap('t_cell_pos.pickle', 100*297.16)
