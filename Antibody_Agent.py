from mesa import Agent
import random
from random import choice
from Antigen_Agent import *

class Antibody_Agent(Agent):
    """
        Antibody Agent

        Parameters
        ----------
        unique_id: int
        model: Model
        bit_string: string
        """
    def __init__(self, unique_id, model, bit_string):
        super().__init__(unique_id, model)
        self.unique_id = unique_id
        self.bit_string = bit_string

    def move(self):
        """
            This function makes a smart move for the Antibody Agent:
                1. Prendo le celle attorno (get_neighborhood)
                2. Per ogni cella vicina, controllo quali agenti contiene.
                    3. Se contiene un agente target, allora mi sposto nella sua cella.
                    4. Altrimenti, scelgo una cella a caso.

            Parameters
            ----------
            /

            Returns
            -------
            Return the new agent position
            """
        antigen = next((agent for agent in self.model.grid.get_neighbors(self.pos, moore=True,
                                                                             include_center=False, radius=2) if
                            isinstance(agent, Antigen_Agent)), None)
        if antigen:
            self.model.grid.move_agent(self, antigen.pos)
            return
        else:
            possible_steps = self.model.grid.get_neighborhood(self.pos, moore=True, include_center=False, radius=2)
            self.model.grid.move_agent(self, random.choice(possible_steps))

    def get_class_name(self):
        return 'Antibody_Agent'
