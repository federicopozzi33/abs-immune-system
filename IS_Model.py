from mesa import Agent, Model
from mesa.time import RandomActivation
from mesa.space import MultiGrid
from mesa.datacollection import DataCollector
import random
from random import choice
from scipy import special
import numpy as np
import itertools
import sys

from Antibody_Agent import *
from Antigen_Agent import *
from AP_Cell_Agent import *
from B_Cell_Agent import *
from Dying_Agent import *
from T_Cell_Agent import *

from pickle_functions import *


# TODO: B-cell mutation module;
# TODO: incrementare la probabilità per un apc di legarsi ad una T rispetto a quella di mangiare un antigene in base al numero di
# TODO: antigeni già mangiati
# TODO: B-cell mutation module;
# TODO: incrementare la probabilità per un apc di legarsi ad una T rispetto a quella di mangiare un antigene in base al numero di
# TODO: antigeni già mangiati
# ASK: perchè all'istante 0 ho 20 T-cell già compatibili?
# ASK: l'uccisione degli antigeni non dipende molto da dove si trovano (es. lontani dagli anticorpi) piuttosto che da quanti anticorpi ci sono?
# ASK: il random walk potrebbe essere da sistemare: per esempio, posso considerare le 8 cella attorno come faccio ora, e poi,
#      sempre in modo randomico estrarre per ogni direzione un numero tra 0 e 1, e sommarlo alle coordinate (in questo modo potrei spostarmi anche di 2)


def count_collector_antibody_list(model):
    """
        This function collect data for analysis

        Parameters
        ----------
        model: Model Mesa obj

        Returns
        -------
        int: Return the length of antibody_list
        """

    agents_list = [agents for agents in model.schedule.agents]
    return len([antibody for antibody in agents_list if isinstance(antibody, Antibody_Agent)])


def count_collector_antigen_list(model):
    """
        This function collect data for analysis

        Parameters
        ----------
        model: Model Mesa obj

        Returns
        -------
        int: Return the length of antigen_list
        """
    agents_list = [agents for agents in model.schedule.agents]
    return len([antigen for antigen in agents_list if isinstance(antigen, Antigen_Agent)])


def count_collector_ap_cell_list(model):
    """
        This function collect data for analysis

        Parameters
        ----------
        model: Model Mesa obj

        Returns
        -------
        int: Return the length of antigen_presenting_cell_list
        """
    agents_list = [agents for agents in model.schedule.agents]
    return len([antigen for antigen in agents_list if isinstance(antigen, AP_Cell_Agent)])


def count_collector_t_cell_list(model):
    """
        This function collect data for analysis

        Parameters
        ----------
        model: Model Mesa obj

        Returns
        -------
        int: Return the length of t_cell_list
        """
    agents_list = [agents for agents in model.schedule.agents]
    return len([t_cell for t_cell in agents_list if isinstance(t_cell, T_Cell_Agent)])


def count_collector_t_cell_binded_with_b(model):
    agents_list = [agent for agent in model.schedule.agents if isinstance(agent, T_Cell_Agent) and not agent.can_bind
                   and isinstance(agent.is_binded_with, B_Cell_Agent)]
    return len(agents_list)


def count_collector_t_cell_binded_with_apc(model):
    agents_list = [agent for agent in model.schedule.agents if isinstance(agent, T_Cell_Agent) and not agent.can_bind
                   and isinstance(agent.is_binded_with, AP_Cell_Agent)]
    return len(agents_list)


def count_collector_b_binded(model):
    agents_list = [agent for agent in model.schedule.agents if isinstance(agent, B_Cell_Agent)
                   and all(agent.has_interacted)]
    return len(agents_list)


def count_collector_apc_binded(model):
    agents_list = [agent for agent in model.schedule.agents if isinstance(agent, AP_Cell_Agent) and
                                                                 agent.is_binded_with]
    return len(agents_list)


def count_collector_t_cell_compatible(model):
    """
        This function collect data for analysis

        Parameters
        ----------
        model: Model Mesa obj

        Returns
        -------
        int: Return the length of t_cell_list that are compatible with antigen (so they can interact each other)
        """
    agents_list = [agents for agents in model.schedule.agents]
    antigen = next((antigen for antigen in agents_list if isinstance(antigen, Antigen_Agent)), None)
    return len([t_cell for t_cell in agents_list if isinstance(t_cell, T_Cell_Agent)
                and model.distance_dictionary[model.mpc][t_cell.bit_string] >= model.min_match]) if model.mpc != '' \
            else 0


def count_collector_b_cell_compatible(model):
    """
        This function collect data for analysis

        Parameters
        ----------
        model: Model Mesa obj

        Returns
        -------
        int: Return the length of t_cell_list that are compatible with antigen (so they can interact each other)
        """
    agents_list = [agents for agents in model.schedule.agents]
    return len([b_cell for b_cell in agents_list if isinstance(b_cell, B_Cell_Agent)
                and model.distance_dictionary[model.first_antigen_bit_string][b_cell.bit_string] >= model.min_match]) if model.first_antigen_bit_string != '' \
            else 0


def count_collector_b_cell_list(model):
    """
        This function collect data for analysis

        Parameters
        ----------
        model: Model Mesa obj

        Returns
        -------
        int: Return the length of b_cell_list
        """
    agents_list = [agents for agents in model.schedule.agents]
    return len([b_cell for b_cell in agents_list if isinstance(b_cell, B_Cell_Agent)])


def count_collector_b_cell_plasma_list(model):
    """
        This function collect data for analysis

        Parameters
        ----------
        model: Model Mesa obj

        Returns
        -------
        int: Return the length of b_cell_list, type 'plasma'
        """
    agents_list = [agents for agents in model.schedule.agents]
    b_cell_list = [b_cell for b_cell in agents_list if isinstance(b_cell, B_Cell_Agent)]
    return len([plasma for plasma in b_cell_list if plasma.state == 'plasma'])


def count_collector_b_cell_interaction_1_2(model):
    """
        This function collect data for analysis

        Parameters
        ----------
        model: Model Mesa obj

        Returns
        -------
        int: Return the length of b-cells list that have interacted only with antigens
        """
    agents_list = [agents for agents in model.schedule.agents]
    return len([b_cell for b_cell in agents_list if isinstance(b_cell, B_Cell_Agent) and b_cell.has_interacted[0] and not b_cell.has_interacted[1]])


def count_collector_b_cell_interaction_2_2(model):
    """
        This function collect data for analysis

        Parameters
        ----------
        model: Model Mesa obj

        Returns
        -------
        int: Return the length of b-cells list that have interacted both with antigens and t-cells
        """
    agents_list = [agents for agents in model.schedule.agents]
    return len([b_cell for b_cell in agents_list if isinstance(b_cell, B_Cell_Agent) and all(b_cell.has_interacted)])


class IM_model(Model):
    """
        T Cell Agent

        Parameters
        ----------
        NB: int # initial number of B cells
        NA: int # initial number of Antigen cells
        NT: int # initial number of T cells
        NAP: int # initial number of AP cells
        width: int
        heigth: int
        min_match: int
        aff_level: double
        aff_enhance: double
        radius: int
        step_injection: list # first position indicates how many (antigens) and the step at which they will be inject
                             # next positions indicate after how many steps the injection will take place once all antigens are removed

        Other Parameters
        ----------------
        unique_id: int (default 0)
        running: boolean (default True) # enables web visualization
        grid: MultiGrid
        schedule: ?
        I_BA_p: list (of probability: holds the probability of interaction between B cell and antigen, respect to min_match and aff_level)

        tau_B_Cell: int # Probability that a B cell dies during a time step
        tau_MB_Cell: int # Probability that a memory B cell dies during a time step
        tau_PB_Cell: int # Probability that a plasma B cell dies during a time step
        tau_T_Cell: int # Probability that a T cell dies during a time step
        tau_AG: int # Probability that an antigen dies during a time step
        tau_AB: int # Probability that an antibody dies during a time step
        tau_AP_Cell: int # Probability that a AP cell dies during a time step

        half_life_dict: dict
        agMultRate: float
        dead_counter: int
        antigens_all_dead: int (default -1)
        to_add_from_bone_marrow: list
        len_trip: int
        distance_dictionary: dict

        n_antigens_killed_by_antibody: int (default 0) # counter variable
        n_antigens_killed_by_B: int (default 0) # counter variable
        count_T_cell: int (default initial number of T cells) # counter variable
        count_compatible_ab: int (default 0)
        count_ab_interacting: int (default 0)

        antigen_inj_radius: int
        n_step: int

        datacollector: DataCollector
        first_antigen_bit_string: string (default '') # after first injection, this value will equal to the the first bit string antigen
        """
    def __init__(self, NB, NT, NAP, width, heigth, min_match, aff_level, aff_enhance, radius, step_antigens_injection, step_max):
        self.unique_id_1 = 1
        self.running = True
        self.num_B_cells = NB
        #### num_Antigens to remove
        self.num_T_cells = NT
        self.num_AP_cells = NAP
        self.grid = MultiGrid(width, heigth, True)
        self.schedule = RandomActivation(self)
        # Minimum number of matching bit for the interaction to occur
        self.min_match = min_match
        # Initialize vector with the interaction probabilities
        self.I_BA_p = self.compute_interaction_probability(self.min_match, aff_level)
        self.I_BA_aff_level = aff_level
        # TODO: rendere parametrico rispetto a tau che viene passata a questa funzione
        # TODO: creare un parametro tau_a per aspettativa di vita antgene
        self.tau_B_Cell = 10 # 7%
        self.tau_MB_Cell = 100 # 1.5%
        self.tau_PB_Cell = 5 # 13%
        self.tau_T_Cell = 10 # 7%
        self.tau_MT_cell = 100 # 1.5%
        # Probability that an antigen duplicates during a time step
        # TODO: in IMMNSIM this is set to 0 by default, so the antigens never duplicate.
        self.tau_AG = 100  # 0.7%
        self.tau_AB = 3  # 7%
        self.tau_AP_Cell = 10
        # Dictionary which contains the half life (Pdie) of each possible cell type
        self.half_life_dict = {}
        self.compute_live_probability()
        # Parameter that hold the probability of an antigen to divide at each time step
        self.agMultRate = 0.01  # 0.01
        # Parameter that holds the number of agents that die at the beginning of each step (not for killed antigens)
        self.dead_counter = 0
        # Parametro che tiene conto del numero di step necessari ad uccidere tutti gli antigeni
        self.antigens_all_dead = -1
        self.antibody_all_dead = -1
        self.to_add_from_bone_marrow = []
        # Number of steps necessary for a new born B-cell to become active
        self.len_trip = 5
        # Number of antibodies that the B-cell produces at every step
        self.num_Ab = 5
        # Dictionary which contains the Hamming distance of every possible pair of 8 bit strings
        self.distance_dictionary = self.compute_distance_dictionary()
        # Attributes for statistics
        self.n_antigens_killed_by_antibody = 0
        self.n_antigens_killed_by_B = 0
        self.n_antigens_killed_by_apc = 0
        #TODO: contatore tmeporaneo T
        self.count_T_cell = NT
        # Parameter which contains the radius of the antigen injection
        self.antigen_inj_radius = radius
        # Parameter to count the number of compatible antibodies
        self.count_compatible_ab = 0
        # Parameter to count the number of antibodies that try to interact with an antigen
        self.count_ab_interacting = 0
        # Parameter to keep track of the step
        self.n_step = 0
        # Parameter to keep track of one of the B-cell bunded with a T
        # self.datacollector = DataCollector(
        #     model_reporters = {"Antibodies": count_collector_antibody_list,
        #                        "Antigens": count_collector_antigen_list,
        #                        "T-Cell": count_collector_t_cell_list,
        #                        "B-Cell": count_collector_b_cell_list,
        #                        "B-Cell-Plasma": count_collector_b_cell_plasma_list,
        #                        "AP-Cell": count_collector_ap_cell_list,
        #                        "B-Cell-I-1": count_collector_b_cell_interaction_1_2,
        #                        "B-Cell-I-2": count_collector_b_cell_interaction_2_2,
        #                        "T-Cell-compatible": count_collector_t_cell_compatible
        #                        },
        #     agent_reporters = {'unique_id': 'unique_id'}
        #     )

        self.datacollector = DataCollector(model_reporters={"Antibodies": count_collector_antibody_list,
                                                            "Antigens": count_collector_antigen_list,
                                                            "T-Cell": count_collector_t_cell_list,
                                                            "B-Cell": count_collector_b_cell_list,
                                                            "B-Cell-Plasma": count_collector_b_cell_plasma_list,
                                                            "AP-Cell": count_collector_ap_cell_list})
                                                            # "T-Cell-compatible": count_collector_t_cell_compatible,
                                                            # "B-Cell-compatible": count_collector_b_cell_compatible,
                                                            # "T-Cell-Binded-with-b": count_collector_t_cell_binded_with_b,
                                                            # "T-Cell-Binded-with-apc": count_collector_t_cell_binded_with_apc,
                                                            # "B-Cell-Binded": count_collector_b_binded,
                                                            # "AP-Cell-Binded": count_collector_apc_binded})

        self.first_antigen_bit_string = ''
        self.step_antigens_injection = step_antigens_injection

        self.step_max = step_max

        # Variable for storing positions of agents --> Purpose: heatmap of the positions
        self.pos_b_cells = []
        self.pos_a_cells = []
        self.pos_t_cells = []
        self.pos_ab_cells = []
        self.pos_ap_cells = []

        self.freq_b_cells = 0
        self.freq_a_cells = 0
        self.freq_t_cells = 0
        self.freq_ab_cells = 0
        self.freq_ap_cells = 0

        self.n_b_cells = 0
        self.n_a_cells = 0
        self.n_t_cells = 0
        self.n_ab_cells = 0
        self.n_ap_cells = 0

        # Useless boolean variable just for heatmap
        self.flag = True
        # Flag for keep track of all_dead variable
        self.can_count_death_steps = False
        self.flag_antibody = False
        # Parameter which holds the mhc string for every bit cell
        self.mhc = ''
        # Parameter which holds the peptide string for every antigen
        self.peptide = ''
        # Parameter which holds the peptide string for every antigen
        self.mpc = ''
        self.initialize_agents()
        self.antibody_radius = 2

        ## NEW
        self.data = []
        self.n_antigens_killed_by_apc = 0
        ## NEW

        self.n_antigens_killed_by_apc_in_this_step = 0
        self.n_antigens_killed_by_antibody_in_this_step = 0
        self.n_antigens_killed_by_B_in_this_step = 0
        # Parameter to keep track of how many antigens injections have been made
        self.n_injection = 0
        

    def generate_unique_id(self):
        self.unique_id_1 += 1
        return self.unique_id_1

    def initialize_agents(self):
        # Create B cell agents
        print("i'm creating agents!")
        # Generate peptide and MHC strings (specific for only one antigen
        mhc_left = self.generate_bit_string(4)
        mhc_right = self.generate_bit_string(4)
        peptide_right = self.generate_bit_string(4)
        peptide_left = ''.join('1' if x == '0' else '0' for x in peptide_right)
        self.mhc = mhc_left + mhc_right
        self.peptide = peptide_left + peptide_right

        for i in range(self.num_B_cells):
            self.create_B_cell(self.generate_unique_id(), self.generate_bit_string(8), self.mhc)

        # Create T-cell
        for i in range(self.num_T_cells):
            self.create_T_cell(self.generate_unique_id(), self.generate_bit_string(8))

        # Create AP-cell
        for i in range(self.num_AP_cells):
            self.create_AP_cell(self.generate_unique_id())

    def get_antigens_injection_borders(self, center_x, center_y):
        left_border = center_x - self.antigen_inj_radius if center_x - self.antigen_inj_radius > 0 else 0
        right_border = center_x + self.antigen_inj_radius if center_x + self.antigen_inj_radius < self.grid.width \
                            else self.grid.width
        top_border = center_y - self.antigen_inj_radius if center_y - self.antigen_inj_radius > 0 else 0
        bottom_border = center_y + self.antigen_inj_radius if center_y + self.antigen_inj_radius < self.grid.height \
                            else self.grid.height
        return [left_border, right_border, top_border, bottom_border]

    def antigens_injection(self, n_antigens):
        print('N antigens to inject: {}'.format(n_antigens))
        # n-th antigens injection
        if self.first_antigen_bit_string:
            print('doing second injection')
            center_x, center_y = random.randrange(self.grid.width), random.randrange(self.grid.height)
            left_border, right_border, top_border, bottom_border = self.get_antigens_injection_borders(center_x, center_y)
            for i in range(1, n_antigens):
                self.create_Antigen(self.generate_unique_id(), self.first_antigen_bit_string, self.peptide,
                                    [left_border, right_border, top_border, bottom_border])
        # it's the first time that antigens will be injected
        else:
            # Initialize MPC complex (it's unique because ther is only one antigen in the model
            self.create_mpc(self.mhc, self.peptide)
            self.create_Antigen(self.generate_unique_id(), self.generate_bit_string(8), self.peptide)
            first_antigen = [x for x in self.schedule.agents if isinstance(x, Antigen_Agent)][0]
            self.first_antigen_bit_string = first_antigen.bit_string
            center_x, center_y = first_antigen.pos
            left_border, right_border, top_border, bottom_border = self.get_antigens_injection_borders(center_x, center_y)
            for i in range(1, n_antigens):
                self.create_Antigen(self.generate_unique_id(), self.first_antigen_bit_string, self.peptide,
                                    [left_border, right_border, top_border, bottom_border])

    def get_antibodies_injection_borders(self, center_x, center_y):
        left_border = center_x - self.antibody_radius if center_x - self.antibody_radius > 0 else 0
        right_border = center_x + self.antibody_radius if center_x + self.antibody_radius < self.grid.width \
                            else self.grid.width
        top_border = center_y - self.antibody_radius if center_y - self.antibody_radius > 0 else 0
        bottom_border = center_y + self.antibody_radius if center_y + self.antibody_radius < self.grid.height \
                            else self.grid.height
        return [left_border, right_border, top_border, bottom_border]

    def generate_bit_string(self, n_bit):
        """
            Utiliy function: it generates all possibile combination of string of fixed length (8) on alphabeth 0 and 1
            """
        return ''.join(choice(['0', '1']) for i in range(n_bit))

    def hamming_distance(self, s1, s2):
        """
            Function that computes the Hamming distance between two given strings

            Parameters:
            -----------
            arg1: string
            arg2: string

            Returns
            -------
            int
            Hamming distance
            """
        return sum(c1 != c2 for c1, c2 in zip(s1, s2))

    def compute_distance_dictionary(self):
        """
            This function computes the Hamming distance of every possible pair of 8 bit strings
            """
        lst = list(map(''.join, list(itertools.product(['0', '1'], repeat=8))))

        dict = {}
        for x in lst:
            dict[x] = {}
            dict2 = {}
            for y in lst:
                dict2[y] = self.hamming_distance(x, y)
            dict[x] = dict2

        return dict

    def compute_live_probability(self):
        """
            # TODO: change function name from compute_live_probability to compute_die_probability?

            This function computes half life for each type of agent respect to its corresponding tau parameter
            """
        self.half_life_dict['B_Cell_Agent'] = 1 if self.tau_B_Cell == 0 else np.exp(np.log(2) * (-1) / self.tau_B_Cell)
        self.half_life_dict['MB_Cell_Agent'] = 1 if self.tau_MB_Cell == 0 else np.exp(np.log(2) * (-1) / self.tau_MB_Cell)
        self.half_life_dict['PB_Cell_Agent'] = 1 if self.tau_PB_Cell == 0 else np.exp(np.log(2) * (-1) / self.tau_PB_Cell)
        self.half_life_dict['T_Cell_Agent'] = 1 if self.tau_T_Cell == 0 else np.exp(np.log(2) * (-1) / self.tau_T_Cell)
        self.half_life_dict['Antigen_Agent'] = 1 if self.tau_AG == 0 else np.exp(np.log(2) * (-1) / self.tau_AG)
        self.half_life_dict['Antibody_Agent'] = 1 if self.tau_AB == 0 else np.exp(np.log(2) * (-1) / self.tau_AB)
        self.half_life_dict['AP_Cell_Agent'] = 1 if self.tau_AP_Cell == 0 else np.exp(np.log(2) * (-1) / self.tau_AP_Cell)
        self.half_life_dict['MT_Cell_Agent'] = 1 if self.tau_MT_cell == 0 else np.exp(np.log(2) * (-1) / self.tau_MT_cell)

    def prepare_heatmap_pos(self, b_list, a_list, antibody_list, t_list, apc_list):
        if b_list:
            self.pos_b_cells.append([b.pos for b in b_list])
        if a_list:
            self.pos_a_cells.append([a.pos for a in a_list])
        if antibody_list:
            self.pos_ab_cells.append([ab.pos for ab in antibody_list])
        if t_list:
            self.pos_t_cells.append([t.pos for t in t_list])
        if apc_list:
            self.pos_ap_cells.append([apc.pos for apc in apc_list])

    def prepare_heatmap_freq(self, b_list, a_list, antibody_list, t_list, apc_list):
        if b_list:
            self.freq_b_cells += 1
        if a_list:
            self.freq_a_cells += 1
        if antibody_list:
            self.freq_ab_cells += 1
        if t_list:
            self.freq_t_cells += 1
        if apc_list:
            self.freq_ap_cells += 1

    def prepare_heatmap_n(self, b_list, a_list, antibody_list, t_list, apc_list):
        self.n_b_cells += len(b_list)
        self.n_a_cells += len(a_list)
        self.n_t_cells += len(antibody_list)
        self.n_ab_cells += len(t_list)
        self.n_ap_cells += len(apc_list)

    def step(self):
        self.n_step += 1
        self.n_antigens_killed_by_apc_in_this_step = 0
        self.n_antigens_killed_by_antibody_in_this_step = 0
        self.n_antigens_killed_by_B_in_this_step = 0
        self.collect_data()
        ## NEW
        for a in self.schedule.agents:
            if not isinstance(a, Dying_Agent):
                if not self.will_die(a):
                    #a.move()
                    # if not isinstance(a, Antigen_Agent):
                    #     a.move()
                    #     a.move()
                    # else:
                    #     a.move()
                    if self.n_step % 1 == 0:
                        a.move()
                    # elif (not isinstance(a, Antigen_Agent)) and (not isinstance(a, Antibody_Agent)):
                    elif not isinstance(a, Antigen_Agent):
                        a.move()
        ## NEW -- Count dead agents during this step
        self.data[-1].append([self.data[-1][1][0][0] - len([agents for agents in self.schedule.agents])])
        ## NEW
        # b_list_1 = [x for x in self.schedule.agents if isinstance(x, B_Cell_Agent)]
        # a_list_1 = [x for x in self.schedule.agents if isinstance(x, Antigen_Agent)]
        # antibody_list_1 = [x for x in self.schedule.agents if isinstance(x, Antibody_Agent)]
        # t_list_1 = [x for x in self.schedule.agents if isinstance(x, T_Cell_Agent)]
        # apc_list_1 = [x for x in self.schedule.agents if isinstance(x, AP_Cell_Agent)]
        # self.prepare_heatmap_n(b_list_1, a_list_1, antibody_list_1, t_list_1, apc_list_1)
        # self.prepare_heatmap_freq(b_list_1, a_list_1, antibody_list_1, t_list_1, apc_list_1)
        # if self.n_step == 1:
        #     b_list_1 = [x.pos for x in b_list_1]
        #     t_list_1 = [x.pos for x in t_list_1]
        #     apc_list_1 = [x.pos for x in apc_list_1]
        #     save_data_with_pickle('b_cell_pos_step1.pickle', b_list_1)
        #     save_data_with_pickle('t_cell_pos_step1.pickle', t_list_1)
        #     save_data_with_pickle('ap_cell_pos_step1.pickle', apc_list_1)
        # if self.n_step == self.step_antigens_injection[0][0]:
        #     a_list_1 = [x.pos for x in a_list_1]
        #     save_data_with_pickle('a_cell_pos_step1.pickle', a_list_1)
        # if antibody_list_1 and self.flag:
        #     antibody_list_1 = [x.pos for x in antibody_list_1]
        #     save_data_with_pickle('ab_cell_pos_step1.pickle', antibody_list_1)
        #     self.flag = False
        if self.n_step == self.step_max:
            # save_data_with_pickle('b_cell_pos.pickle', self.pos_b_cells)
            # save_data_with_pickle('a_cell_pos.pickle', self.pos_a_cells)
            # save_data_with_pickle('ab_cell_pos.pickle', self.pos_ab_cells)
            # save_data_with_pickle('t_cell_pos.pickle', self.pos_t_cells)
            # save_data_with_pickle('ap_cell_pos.pickle', self.pos_ap_cells)
            # print('B Cells: {} | A Cells: {} | AB Cells: {} | T Cells: {} | AP Cells: {}'.format(self.freq_b_cells, self.freq_a_cells, self.freq_ab_cells, self.freq_t_cells, self.freq_ap_cells))
            # print('# B Cells: {}'.format(self.n_b_cells / self.freq_b_cells))
            save_data_with_pickle(self.data, "simulation_data.pickle")
            sys.exit()

        print('Numero di agenti presenti: {}'.format(len(self.schedule.agents)))
        #print(max([x.unique_id for x in self.schedule.agents]))
        #print('Siamo allo step: {}, Il prossimo step di injection è: {}'.format(self.n_step, self.step_antigens_injection[0][0]))
        antigens_left = [x for x in self.schedule.agents if isinstance(x, Antigen_Agent)]
        antibody_left = [x for x in self.schedule.agents if isinstance(x, Antibody_Agent)]
        self.flow()
        self.flow_new_cells()

        # NEW -- Count new agents from bone marrow
        self.data[-1].append(
            [(len([agents for agents in self.schedule.agents])) - (self.data[-1][1][0][0] - self.data[-1][-1][0])])

        if self.can_count_death_steps and len(antigens_left) == 0:
            self.antigens_all_dead = self.n_step
            self.flag_antibody = True
            self.can_count_death_steps = False
            print('##################### Antingens has been defeated at step {}'.format(self.antigens_all_dead))
        if self.flag_antibody and len(antigens_left) == 0 and len(antibody_left) == 0:
            self.antibody_all_dead = self.n_step
            self.flag_antibody = False
            print('##################### Antibody has been defeated at step {}'.format(self.antibody_all_dead))
            if len(self.step_antigens_injection) == 0:
                save_data_with_pickle("simulation_data.pickle", self.data)
                sys.exit()
            
        if len(self.step_antigens_injection) > 0 and self.step_antigens_injection[0][0] == 0 \
                and len(antibody_left) == 0 and len(antigens_left) == 0:
            print('############### Antigens injection at step: {}'.format(self.n_step))
            self.can_count_death_steps = True
            self.antigens_injection(self.step_antigens_injection[0][1])
            self.step_antigens_injection = self.step_antigens_injection[1:]
        elif len(self.step_antigens_injection) > 0 and len(antibody_left) == 0 and len(antigens_left) == 0:
            self.step_antigens_injection[0][0] -= 1

        self.datacollector.collect(self)
        self.count_compatible_ab = 0
        self.count_ab_interacting = 0
        # self.to_add_from_bone_marrow = list(map(lambda x: [x[0], x[1] - 1], self.to_add_from_bone_marrow))
        # Remove the agents that died two steps before; decrease the counter of the antigens thta died the step before
        dying = [x for x in self.schedule.agents if isinstance(x, Dying_Agent) and x.count == 0]
        for d in dying:
            self.schedule.remove(d)
            self.grid.remove_agent(d)
        dying = [x for x in self.schedule.agents if isinstance(x, Dying_Agent) and x.count == 1]
        for d in dying:
            d.count -= 1

        self.dead_counter = 0

        if antigens_left:
            self.clone_antigen(antigens_left)
        # t_cells_to_clone = [x for x in self.schedule.agents if isinstance(x, T_Cell_Agent)
        #                     and not x.can_bind and x.divide_T_counter < x.divide_T_max]
        # self.clone_T_cell(t_cells_to_clone)
        # TODO: sistemare questo
        # map(self.will_live, self.schedule.agents)
        # print('Final agents: {}'. format(len(self.schedule.agents)))

        # self.schedule.step()
        # for i in self.grid.coord_iter():
        #     print(i)
        # count_compatible = 0
        for i in self.grid.coord_iter():
            b_list = [x for x in list(i[0]) if isinstance(x, B_Cell_Agent)]
            a_list = [x for x in list(i[0]) if isinstance(x, Antigen_Agent)]
            antibody_list = [x for x in list(i[0]) if isinstance(x, Antibody_Agent)]
            t_list = [x for x in list(i[0]) if isinstance(x, T_Cell_Agent)]
            apc_list = [x for x in list(i[0]) if isinstance(x, AP_Cell_Agent)]
            #self.prepare_heatmap_pos(b_list, a_list, antibody_list, t_list, apc_list)
            if len(apc_list) > 0 and len(a_list) > 0:
                for apc in apc_list:
                    if not apc.has_interacted:
                        # The apc has not interacted
                        a_list = self.interaction_apc_antigen(apc, a_list)
                    elif not apc.is_binded_with:
                        # TODO: aggiungere forse la scelta di comportamento in questo caso (aggiungere max
                        # TODO: n da mangiare)
                        self.interaction_apc_T(apc, t_list)
            if len(t_list) > 0:
                for t in t_list:
                    if not t.can_bind:
                        if t.divide_T_counter < t.divide_T_max:
                            self.clone_T_cell(t)
                        elif isinstance(t.is_binded_with, AP_Cell_Agent):
                            self.t_specialization(t)
            # count_compatible += self.check_number_compatible_pairs(a_list, b_list)
            # 1: check if the cell contains any b-cell. If not, nothing happens in the cell
            if len(b_list) > 0:
                for b in b_list:
                    # Check if the B-cell is naive
                    if b.state == 'naive' or b.state == 'memory':
                        # TODO: handle the other B cell possible states
                        if not b.has_interacted[0]:
                            if len(a_list) > 0:
                                a_list = self.interaction_with_antigen(b, a_list)
                        else:
                            if all(b.has_interacted):
                                # Case when B has interacted with both antigen and T
                                if b.divide_B_counter < b.divide_B_max:
                                    b.divide_B_counter += 1
                                    self.clone_b_cell(b)
                                else:
                                    self.b_specialization(b)
                                    # Aggiungere distacco dalla T
                            #else:
                                # # Case when B has interacted with an antigen but not with a T yet:
                                # if len(a_list) > 0 and len(t_list) > 0:
                                #     # Extract a random number to decide the action of the B-cell: interact with T or ant
                                #     #if random.randrange(0, 10) <= 4:
                                #     if True:
                                #         # the cell will interact with a T-cell
                                #         self.interaction_with_T(b, t_list)
                                #     # else:
                                #     #     # The cell will interact with an antigen
                                #     #     a_list = self.interaction_with_antigen(b, a_list)
                                # elif len(a_list) > 0:
                                #     # Case when there are no T -> B can only try to interact with an antigen
                                #     a_list = self.interaction_with_antigen(b, a_list)
                            elif len(t_list) > 0:
                                # Case when there are no antigens -> B can only try to interact with a T
                                self.interaction_with_T(b, t_list)
                            else:
                                # No T and no antigen -> do nothing
                                pass
                    elif b.state == 'plasma':
                        # Case where the B has become plasma, thus it can start thr antibody production
                        for i in range(0, self.num_Ab):
                            self.create_Antibody(self.generate_unique_id(), b.bit_string, b.pos)

                if len(antibody_list) > 0 and len(a_list) > 0:
                    # print('Lunghezza anticorpi: {}; Lunghezza antigeni: {}'.format(len(antibody_list), len(a_list)))
                    for ab in antibody_list:
                        self.interaction_with_antigen(ab, a_list, ab)
        ## NEW
        self.data[-1].append(
            [self.n_antigens_killed_by_antibody, self.n_antigens_killed_by_antibody_in_this_step])
        self.data[-1].append([self.n_antigens_killed_by_B, self.n_antigens_killed_by_B_in_this_step])
        self.data[-1].append([self.n_antigens_killed_by_apc, self.n_antigens_killed_by_apc_in_this_step])
        ## NEW
        #count_antigens_left(self)
        # print('!!!!!!!!!!!!! compatible in the same cell: {}'.format(count_compatible))


    def flow_new_cells(self):
        """
        This function does this:
            1. Takes the couples (num_of_cells_to_create, steps_left_to_birth) with steps_left = 0 and creates num_of_cells new B-cells
            2. Deletes all the couples that passed the first condition
            3. For the couples with more than 0 steps left, decreases their second element
            """
        for x in self.to_add_from_bone_marrow:
            if x[1] == 0:
                for i in range(0, x[0]):
                    if x[2] == 'B_Cell_Agent':
                        self.create_B_cell(self.generate_unique_id(),
                                           self.generate_bit_string(8), self.mhc)
                    elif x[2] == 'AP_Cell_Agent':
                        self.create_AP_cell(self.generate_unique_id())
                    else:
                        self.create_T_cell(self.generate_unique_id(),
                                           self.generate_bit_string(8))
        for x in self.to_add_from_bone_marrow:
            if x[1] == 0:
                self.to_add_from_bone_marrow.remove(x)
        for i, x in enumerate(self.to_add_from_bone_marrow):
            self.to_add_from_bone_marrow[i][1] = x[1] - 1

    def flow(self):
        """
        # TODO: Perchè aggiungo qualcosa all'array anche quando non devo creare nulla?

        This functions defines and updates to_add_from_bone_marrow list in the following way:
                1. If the probability of dying of an agent is equal to 0, then updates the list like this:
                    [0, len_trip, agent type]
                2. Otherwise:
                    [number of cell to create (using formula), len_trip, agent type]
            The formula depends on initial number of agent type in the simulation and tau's agent
            """
        # print('Numero di b-cell da creare: {}'.format(np.round(np.log(2) * self.num_B_cells / self.tau_B_Cell)))
        if self.tau_B_Cell == 0:
            self.to_add_from_bone_marrow.append([0, self.len_trip, 'B_Cell_Agent'])
        else:
            self.to_add_from_bone_marrow.append([int(np.round(np.log(2) * self.num_B_cells / self.tau_B_Cell)),
                                             self.len_trip, 'B_Cell_Agent'])
        # print('Numero di T-cell da creare: {}'.format(np.round(np.log(2) * self.num_T_cells / self.tau_T_Cell)))
        if self.tau_T_Cell == 0:
            self.to_add_from_bone_marrow.append([0, self.len_trip, 'T_Cell_Agent'])
        else:
            self.to_add_from_bone_marrow.append([int(np.round(np.log(2) * self.num_T_cells / self.tau_T_Cell)),
                                             self.len_trip, 'T_Cell_Agent'])
        if self.tau_AP_Cell == 0:
            self.to_add_from_bone_marrow.append([0, self.len_trip, 'AP_Cell_Agent'])
        else:
            self.to_add_from_bone_marrow.append([int(np.round(np.log(2) * self.num_AP_cells / self.tau_AP_Cell)),
                                                 self.len_trip, 'AP_Cell_Agent'])

    def will_die(self, a):
        """
            This function decides if an agent will die or not. If it will, removes the agent from the simulation, and resets some parameters
            """
        if random.uniform(0, 1) > self.half_life_dict[a.get_class_name()]:
            if isinstance(a, T_Cell_Agent):
                # A T-cell is dying, so if it is linked to a B, this one needs to be set free
                self.count_T_cell -= 1
                if isinstance(a.is_binded_with, B_Cell_Agent):
                    # The T-cell dying is binded with a B-cell, thus this one needs to be set free.
                    # The B-cell will keep exposing the antigen that it had previously internalized
                    a.is_binded_with.is_binded_with = None
                    a.is_binded_with.has_interacted = [True, False]
                elif isinstance(a.is_binded_with, AP_Cell_Agent):
                    # The T-cell dying is binded with a AP-cell, thus this one needs to be set free.
                    a.is_binded_with.has_interacted = False
                    a.is_binded_with.mpc = ''
                    a.is_binded_with.is_binded_with = None
            if isinstance(a, B_Cell_Agent) or isinstance(a, AP_Cell_Agent):
                # A B-cell is dying, so if it is linked to a T, this one needs to be set free
                if a.is_binded_with:
                    a.is_binded_with.can_bind = True
                    a.is_binded_with.is_binded_with = None

            self.dead_counter += 1

            self.schedule.remove(a)
            self.grid.remove_agent(a)
            return True
        else:
            return False

    def collect_data(self):
        agents = [agents for agents in self.schedule.agents]
        n_agents = len(agents)

        antibody = [antibody for antibody in agents if isinstance(antibody, Antibody_Agent)]
        antigen = [antigen for antigen in agents if isinstance(antigen, Antigen_Agent)]

        t_cell = [t_cell for t_cell in agents if isinstance(t_cell, T_Cell_Agent)]
        t_cell_binded_with_b = len([t for t in t_cell if isinstance(t.is_binded_with, B_Cell_Agent)])
        t_cell_binded_with_apc = len([t for t in t_cell if isinstance(t.is_binded_with, AP_Cell_Agent)])
        t_cell_memory = len([t for t in t_cell if t.state == 'memory'])
        t_cell_naive = len([t for t in t_cell if t.state == 'naive'])
        t_cell_compatible = len(
            [t for t in t_cell if self.distance_dictionary[self.mpc][t.bit_string] >= self.min_match]) if self.mpc != '' \
            else 0
        t_cell_dividing = len([t for t in t_cell if t.divide_T_counter != 0])

        ap_cell = [apc for apc in agents if isinstance(apc, AP_Cell_Agent)]
        ap_cell_binded_with_t = len([apc for apc in ap_cell if apc.is_binded_with])

        b_cell = [b_cell for b_cell in agents if isinstance(b_cell, B_Cell_Agent)]
        b_cell_compatible = len([b for b in b_cell if self.distance_dictionary[self.first_antigen_bit_string][
            b.bit_string] >= self.min_match]) if self.first_antigen_bit_string != '' \
            else 0
        b_cell_plasma = len([b for b in b_cell if b.state == 'plasma'])
        b_cell_memory = len([b for b in b_cell if b.state == 'memory'])
        b_cell_naive = len([b for b in b_cell if b.state == 'naive'])
        b_cell_1_2 = len([b for b in b_cell if b.has_interacted[0] and not b.has_interacted[1]])
        b_cell_2_2 = len([b for b in b_cell if all(b.has_interacted)])
        b_cell_dividing = len([b for b in b_cell if b.divide_B_counter != 0])

        self.data.append([self.n_step, [[n_agents], [len(antibody)], [len(antigen)],
                                        [len(t_cell), t_cell_binded_with_b, t_cell_binded_with_apc, t_cell_memory,
                                         t_cell_naive, t_cell_compatible, t_cell_dividing],
                                        [len(ap_cell), ap_cell_binded_with_t],
                                        [len(b_cell), b_cell_compatible, b_cell_plasma, b_cell_memory, b_cell_naive,
                                         b_cell_1_2, b_cell_2_2, b_cell_dividing]]])

    def clone_antigen(self, antigens_list):
        """
            This function clones antigens using a probability equal to agMultRate parameter
            """
        for a in antigens_list:
            if random.uniform(0, 1) <= self.agMultRate:
                # The antigen a divides
                self.create_Antigen(self.generate_unique_id(), a.bit_string, a.peptide, a.pos)

    def clone_T_cell(self, t_father):
        """
            This function clones T cells
            """
        # TODO: aggiungere la probabilità di duplicarsi
        for i in range(int(t_father.cells_to_create[0])):
            t = self.create_T_cell(self.generate_unique_id(),
                                   t_father.bit_string, t_father.pos)
            t.state = 'memory'
        t_father.divide_T_counter += 1
        t_father.cells_to_create = t_father.cells_to_create[1:] if len(t_father.cells_to_create) > 1 else ''

    def t_specialization(self, t):
        t.can_bind = True
        t.divide_T_counter = 0
        t.state = 'memory'
        t.is_binded_with.is_binded_with = None
        t.is_binded_with.mpc = ''
        t.is_binded_with.has_interacted = False
        t.cells_to_create = '124'
        t.is_binded_with = None


    def compute_interaction_probability(self, min_match, aff_level):
        """
            This function initializes a list that contains probabilities of interaction between B cells and antigens
            """
        # N.B. Temporary: this should contain the probabilities
        # TODO: USARE LA FORMULA
        ISp = []
        for i in range(0, 9):
            if i < min_match:
                ISp.append(0)
            elif i == min_match:
                ISp.append(i)
            else:
                ISp.append(i)
        return ISp

    def interaction_with_antigen(self, b, a_list, *args):
        """
            TODO: non ha senso come viene chiamata: gli passo negli Args la stessa lista di anticorpi che già gli passo....

            This function handles the interaction between a specific B cell and a list of antigens
            and the interaction between an antibody and a list of antigens

            Parameters:
            -----------
            b: B cell obj # B-cell that needs to interact with an antigen
            a_list: list (of antigens)
            *args: just for antibody-antigens interaction

            # Both not empty
            """
        flag = False
        i = 0

        while i < len(a_list) and not flag:
            # print(a_list)
            a = a_list[i]
            # Check that the match level between b and the current antigen is sufficient for the interaction
            match = self.distance_dictionary[b.bit_string][a.bit_string]
            if args:
                self.count_ab_interacting += 1
            if match >= self.min_match:
                if args:
                    self.count_compatible_ab += 1
                if self.I_BA_p[match] > 0:
                    # Extract a random number. If that number is lower than the match level the interaction occurs
                    if random.randrange(0, 9) <= match:
                    # if True:
                        # Create a new agent il order to make the death visible
                        dying = Dying_Agent(self.generate_unique_id(), self)
                        self.schedule.add(dying)
                        x, y = a.pos
                        self.grid.place_agent(dying, (x, y))
                        # Remove the antigen
                        #print('What i\'m removing: {}'.format(a.unique_id))
                        self.schedule.remove(a)
                        self.grid.remove_agent(a)
                        a_list.remove(a)
                        if args:
                            #print('What i\'m removing: {}'.format(args[0].unique_id))
                            # Case when the cell that interacted with the antigen is an antibody
                            self.schedule.remove(args[0])
                            self.grid.remove_agent(args[0])
                            self.n_antigens_killed_by_antibody += 1
                            self.n_antigens_killed_by_antibody_in_this_step += 1
                        else:
                            # Case when the cell that interacted with the antigen is a B-cell
                            self.n_antigens_killed_by_B += 1
                            self.n_antigens_killed_by_B_in_this_step += 1
                            b.has_interacted[0] = True
                            # Set the value to remember the antigen to which it has bounded
                            b.mpc = self.mpc
                        return a_list
            i += 1
        return a_list

    def interaction_apc_antigen(self, apc, a_list):
        """
            This function handles the interaction between an AP cell and a list of antigens
            """
        flag = False
        i = 0

        while i < len(a_list) and not flag:
            a = a_list[i]
            if random.uniform(0, 1) <= 0.5:
                # Make the apc cell interact with the antigen
                apc.has_interacted = True
                # When an APC binds with an antigen, half of its MHC molecule binds with half of the antigen's peptide
                # string. The remaining two halves (the MPC complex) are exposed on the APC's surface and are then
                # recognized by the T-cell
                apc.mpc = self.mpc
                dying = Dying_Agent(self.generate_unique_id(), self)
                self.schedule.add(dying)
                x, y = a.pos
                self.grid.place_agent(dying, (x, y))
                # Remove the antigen
                self.schedule.remove(a)
                self.grid.remove_agent(a)
                a_list.remove(a)
                self.n_antigens_killed_by_apc += 1
                self.n_antigens_killed_by_apc_in_this_step += 1
                flag = True
            i += 1
        return a_list

    def create_mpc(self, mhc, peptide):
        self.mpc = mhc[:4] + peptide[4:]

    def clone_b_cell(self, father):
        """
            Parameters:
            ----------
            father: b_cell which has to undergo cell division
            """
        # TODO: decidere se modificare i bit di compatibilità per la nuova B che nasce da una che si è legata
        # TODO: quando si divide se ne creano 4 o 2? Quella originale legata a T sopravvive?
        # TODO: se la cellula si dividesse in 4, conta come uno o 4 step?
        # This function is called when a B has binded with a T and is reproducing: for this reason, each cell that is
        # produced here needs to be immediatly specialized into either memory or plasma. If it becomes memory it
        # will continue to live as a naive B-cell which hasn't bounded yet with an antigen.
        # TODO: this change will be important when we will add more than one antigen
        #print('######## ID: {}'.format(father.unique_id))
        #print('###### Numero di B-Cell da creare: {}'.format(father.cells_to_create))
        #print('# Divide MAX: {} | Divide counter: {}'.format(father.divide_B_max, father.divide_B_counter))
        for i in range(int(father.cells_to_create[0])):
            #print('Numero di B-Cell da creare: {} | indice: {}'.format(father.cells_to_create[0], i))
            b = self.create_B_cell(self.generate_unique_id(), father.bit_string, father.mhc, father.pos)
            self.b_specialization(b)
        father.cells_to_create = father.cells_to_create[1:] if len(father.cells_to_create) > 1 else ''
        #print('Numero di B-Cell da creare: {}'.format(father.cells_to_create))
        #print('# Divide MAX: {} | Divide counter: {}'.format(father.divide_B_max, father.divide_B_counter))
        #print('------')


        # b = B_Cell_Agent(max([x.unique_id for x in self.schedule.agents]) + 1000, self)
        # self.schedule.add(b)
        #
        # # Add B-cell to the father's cell
        # x, y = father.pos
        # self.grid.place_agent(b, (x, y))

    def create_Antibody(self, id, bit_string, pos):
        ab = Antibody_Agent(id, self, bit_string)
        self.schedule.add(ab)
        left_border, right_border, top_border, bottom_border = self.get_antibodies_injection_borders(pos[0], pos[1])
        x = random.randrange(left_border, right_border)
        y = random.randrange(top_border, bottom_border)
        # if not args:
        #     x = random.randrange(self.grid.width)
        #     y = random.randrange(self.grid.height)
        # else:
        self.grid.place_agent(ab, (x, y))

    def create_B_cell(self, id, bit_string, mhc, *args):
        b = B_Cell_Agent(id, self, bit_string, mhc)
        self.schedule.add(b)

        if not args:
            x = random.randrange(self.grid.width)
            y = random.randrange(self.grid.height)
        else:
            x, y = args[0]
        self.grid.place_agent(b, (x, y))
        return b

    def create_AP_cell(self, id, *args):
        ap = AP_Cell_Agent(id, self)
        self.schedule.add(ap)

        if not args:
            x = random.randrange(self.grid.width)
            y = random.randrange(self.grid.height)
        else:
            x, y = args[0]
        self.grid.place_agent(ap, (x, y))
        return ap

    def create_T_cell(self, id, bit_string, *args):
        t = T_Cell_Agent(id, self, bit_string)
        self.schedule.add(t)

        if not args:
            x = random.randrange(self.grid.width)
            y = random.randrange(self.grid.height)
        else:
            x, y = args[0]
        self.grid.place_agent(t, (x, y))
        return t

    def create_Antigen(self, id, bit_string, peptide, *args):
        a = Antigen_Agent(id, self, bit_string, peptide)
        self.schedule.add(a)
        # TODO: l'antigene va posizionato nella stessa cella del padre oppure  nelle celle immediatamente vicine?
        if not args:
            x = random.randrange(self.grid.width)
            y = random.randrange(self.grid.height)
        else:
            if len(args[0]) == 4:
                # Place the antigen in a neighborhood of the cell where the first antigen was injected
                # This is the case when the antigens are first injected
                borders = args[0]
                x = random.randrange(borders[0], borders[1])
                y = random.randrange(borders[2], borders[3])
            else:
                # This is the case when an antigen is dividing -> give the new antigen its father's position
                x, y = args[0]
        self.grid.place_agent(a, (x, y))



    def b_specialization(self, b):
        """
            Input:
            b: b-cell which has reached its max number of divisions and needs to specialize into either plasma or memory
        """
        # TODO: pm_ratio (assegnarlo come numero causale alla creazione, e poi qua dentro fare l'estrazione
        b.state = 'plasma' if random.randrange(0, 10) > 4 else 'memory'
        # If b has specialized into a memory cell, both its flags need to be set again to False, so that it can interact
        # again with an antigen
        if b.state == 'memory':
            b.has_interacted = [False, False]
        # TODO: le memory cell devono aumetare il livello di compatibilità?
        # Se la cellula che si sta specializzando ha raggiunto il numero massimo di divisioni
            if b.divide_B_counter == b.divide_B_max:
                b.cells_to_create = '124'
                b.is_binded_with.can_bind = True
                b.is_binded_with.is_binded_with = None

                b.divide_B_counter = 0
                b.is_binded_with.divide_T_counter = 0
                b.is_binded_with.state = 'memory'
                b.is_binded_with.cells_to_create = '124'
                b.is_binded_with = None



    def interaction_with_T(self, b, t_list):
        t_list_available = [x for x in t_list if x.can_bind]
        flag = True
        i = 0
        while i < len(t_list_available) and flag:
            t = t_list_available[i]
            match = self.distance_dictionary[b.mpc][t.bit_string]
            # print('!!!!!!!!!!!!!!!!!!!!! MATCH: {}'.format(match))
            if match >= self.min_match:
                # if True:
                if random.randrange(0, 9) <= match:
                    b.is_binded_with = t
                    t.can_bind = False
                    t.is_binded_with = b
                    b.has_interacted[1] = True
                    flag = False
            i += 1

    def interaction_apc_T(self, apc, t_list):
        t_list_available = [x for x in t_list if x.can_bind]
        flag = True
        i = 0
        while i < len(t_list_available) and flag:
            t = t_list_available[i]
            match = self.distance_dictionary[apc.mpc][t.bit_string]
            if match >= self.min_match:
                # if True:
                if random.randrange(0, 9) <= match:
                    apc.is_binded_with = t
                    t.can_bind = False
                    t.is_binded_with = apc
                    flag = False
            i += 1

def count_antigens_left(model):
    """
        This function collect data for analysis and debug
        """
    antigen_count = 0
    mem_count = 0
    plasma_count = 0
    naive_count = 0
    b_count = 0
    antibody_count = 0
    t_count = 0
    t_bind_count = 0
    sum_match = 0
    print('numero agenti totali per ogni step: {}'.format(len(model.schedule.agents)))
    for i in model.schedule.agents:
        if isinstance(i, Antigen_Agent):
            antigen_count += 1
        if isinstance(i, B_Cell_Agent):
            b_count += 1
            if i.state == 'memory':
                mem_count += 1
            elif i.state == 'plasma':
                plasma_count += 1
            else:
                naive_count += 1
        if isinstance(i, Antibody_Agent):
            antibody_count += 1

        if isinstance(i, T_Cell_Agent):
            t_count += 1
            if not i.can_bind:
                t_bind_count += 1

    print('STEP NUMBER: {}'.format(model.n_step))
    print('Antigens left: {}'.format(antigen_count))
    print('Total b-cell: {}; Number of plasma: {}; Number of naive: {}; Number of memory: {}'.format(b_count, plasma_count, naive_count, mem_count))
    print('Number of T-cells: {}; binded: {}; not binded: {}'.format(t_count, t_bind_count, t_count - t_bind_count))
    if len(model.schedule.agents) != 0:
        print('Percentage of dead cells: {}'.format(model.dead_counter / len(model.schedule.agents)))
    else:
        print('Percentage of dead cells: {}'.format(0))
    print('Number of dead cells: {}'.format(model.dead_counter))
    print('Number of antibody cell: {}'.format(antibody_count))
    print('Number of antigens killed by a B cell: {}'.format(model.n_antigens_killed_by_B))
    print('Number of antigens killed by an antibody: {}'.format(model.n_antigens_killed_by_antibody))
    print('Number of T killed: {}'.format(model.count_T_cell))
    print('NUmber of compatible antibodies: {}'.format(model.count_compatible_ab))
    print('Number of antibodies trying to interact: {}'.format(model.count_ab_interacting))
    print('-----------------------------------------------------------------------')
