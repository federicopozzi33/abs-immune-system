from mesa import Agent
import random
from random import choice


class B_Cell_Agent(Agent):
    """
        B Cell Agent

        Parameters
        ----------
        unique_id: int
        model: Model
        bit_string: string

        Other Parameters
        ----------------
        divide_B_max: int (default 3)
        state: string (default 'naive')
        has_interacted: list of two boolean (default [False, False])
        divide_B_counter: int (default 0)
        is_binded_with: reference to T Cell obj (default None)
        mpc: string (default '')
        """
    def __init__(self, unique_id, model, bit_string, mhc):
        super().__init__(unique_id, model)
        # self.interact = False
        self.unique_id = unique_id
        # Receptor bit string
        self.bit_string = bit_string
        # MHC bit string
        self.mhc = mhc
        # TODO: aggiungere il passaggio di tutti i seguenti parametri:
        # number of max times the cell can divide
        self.divide_B_max = 3
        # number of cell to create at each step during the reproduction phase
        self.cells_to_create = '124'
        # State of the B-cell: either naive, plasma, or memory. At the beginning it is naive
        self.state = 'naive'
        # Flag that states: 1. whether the B-cell has already interacted with an antigen (-> it can interact with T)
        #                   2. whether the B-cell has already interacted with a T-cell (-> it can start to divide)
        self.has_interacted = [False, False]
        # Counter for the number of divisions
        self.divide_B_counter = 0
        # Parameter to hold the reference of the T-cell to which B has bounded
        self.is_binded_with = None
        # Parameter to hold the bit string of the antigen to which B has bounded
        self.mpc = ''

    def move(self):
        possible_steps = self.model.grid.get_neighborhood(self.pos, moore=True, include_center=False)
        new_position = random.choice(possible_steps)
        self.model.grid.move_agent(self, new_position)
        if self.is_binded_with is not None:
            self.model.grid.move_agent(self.is_binded_with, new_position)

    def get_class_name(self):
        return 'B_Cell_Agent' if self.state == 'naive' else ('MB_Cell_Agent' if self.state == 'memory' else 'PB_Cell_Agent')
