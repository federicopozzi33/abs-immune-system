from mesa.visualization.modules import CanvasGrid
from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.modules import ChartModule
from mesa.visualization.UserParam import UserSettableParameter
from IS_Model import *

# default_value, min_value, max_value, increment
n_b_cells = UserSettableParameter('slider', "Number of B Cells", 256, 0, 2000, 1)
n_antigens = UserSettableParameter('slider', "Number of Antigens", 2000, 0, 10000, 1)
n_t_cells = UserSettableParameter('slider', "Number of T Cells", 256, 0, 2000, 2)
n_ap_cells = UserSettableParameter('slider', "Number of AP Cells", 100, 0, 2000, 2)
radius = UserSettableParameter('slider', "Radius injection antigen", 2, 1, 30, 1)

def agent_portrayal(agent):
    portrayal = {"Shape": "circle",
        "Filled": "true"}

    if isinstance(agent, B_Cell_Agent):
        portrayal["Layer"] = 0
        portrayal["r"] = 0.5
        if all(agent.has_interacted):
            portrayal["Color"] = "red"
            portrayal["r"] = 1
        elif agent.has_interacted[0]:
            portrayal["Color"] = "orange"
        else:
            portrayal["Color"] = "yellow"
    elif isinstance(agent, T_Cell_Agent):
        portrayal["Layer"] = 1
        portrayal["r"] = 0.4
        if agent.can_bind:
            portrayal["Color"] = "green"
        else:
            portrayal["Color"] = "blue"
    elif isinstance(agent, Antigen_Agent):
        portrayal["Color"] = "black"
        portrayal["Layer"] = 2
        portrayal["r"] = 0.3
    elif isinstance(agent, Antibody_Agent):
        portrayal["Color"] = "purple"
        portrayal["Layer"] = 2
        portrayal["r"] = 0.2
    elif isinstance(agent, Dying_Agent):
        portrayal["Color"] = "brown"
        portrayal["Layer"] = 3
        portrayal["Shape"] = "rect"
        portrayal["h"] = 1
        portrayal["w"] = 1
    elif isinstance(agent, AP_Cell_Agent):
        portrayal["Color"] = "#80ccff"
        portrayal["Layer"] = 4

        portrayal["Shape"] = "rect"
        portrayal["h"] = 0.5
        portrayal["w"] = 0.5
        # portrayal["Shape"] = "arrowHead"
        # portrayal["Scale"] = 0.33
        # portrayal["heading_x"] = 1
        # portrayal["heading_y"] = 1
    return portrayal


grid = CanvasGrid(agent_portrayal, 50, 50, 700, 700)
#
chart = ChartModule([{"Label": "Antibodies",
                     "Color": "Purple"},
                     {"Label": "Antigens",
                     "Color": "Black"},
                     {"Label": "T-Cell",
                     "Color": "Green"},
                     {"Label": "B-Cell",
                     "Color": "Red"},
                     {"Label": "B-Cell-Plasma",
                     "Color": "Orange"},
                     {"Label": "AP-Cell",
                      "Color": "#80ccff"}
                     ],
                    data_collector_name='datacollector')
#
# chart2 = ChartModule([{"Label": "B-Cell-I-1",
#                      "Color": "Red"},
#                      {"Label": "B-Cell-I-2",
#                      "Color": "Orange"},
#                      {"Label": "B-Cell",
#                      "Color": "Yellow"}],
#                      data_collector_name='datacollector')
#
# chart3 = ChartModule([{"Label": "T-Cell-compatible",
#                      "Color": "Red"}],
#                      data_collector_name='datacollector')
#
# chart4 = ChartModule([{"Label": "T-Cell-compatible",
#                      "Color": "Green"},
#                       {"Label": "B-Cell-compatible",
#                        "Color": "Red"}
#                       ],
#                     data_collector_name='datacollector')
#
# chart5 = ChartModule([{"Label": "T-Cell-Binded-with-b",
#                      "Color": "Blue"},
#                     {"Label": "B-Cell-Binded",
#                      "Color": "yellow"}
#                       ],
#                     data_collector_name='datacollector')
#
# chart6 = ChartModule([{"Label": "T-Cell-Binded-with-apc",
#                      "Color": "Blue"},
#                     {"Label": "AP-Cell-Binded",
#                      "Color": "yellow"}
#                       ],
#                     data_collector_name='datacollector')


server = ModularServer(IM_model,
                       [grid, chart],
                       "IM_Model",
                       {"NB": n_b_cells, "NT": n_t_cells, "NAP": n_ap_cells, "width": 50, "heigth": 50, "min_match": 7, "aff_level": 0.3,
                        "aff_enhance": 0.04, "radius": radius, "step_antigens_injection": [[1, 2000], [1, 4000]], "step_max": 2000}
                       )
